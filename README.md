# odoacer

## Building

- Ensure a `main.py` and `buildozer.spec` exist in the app folder
- Android debug build: `buildozer -v android debug`

### Troubleshooting

- Buildozer sdkmanager cannot find class: XmlSchema
- Solution: 
  - Open `sdkmanager` in editor
  - Change: `DEFAULT_JVM_OPTS='"-Dcom.android.sdklib.toolsdir=$APP_HOME"'`
  - To: `DEFAULT_JVM_OPTS='"-Dcom.android.sdklib.toolsdir=$APP_HOME" -XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee'`
  - May also work: `DEFAULT_JVM_OPTS='"-Dcom.android.sdklib.toolsdir=$APP_HOME" --add-modules java.xml.bind'`
  - Pay attention to the quotation marks
