sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get install -y build-essential ccache git libncurses5:i386 libstdc++6:i386 libgtk2.0-0:i386 libpangox-1.0-0:i386 libpangoxft-1.0-0:i386 libidn11:i386 python2.7 python2.7-dev openjdk-8-jdk unzip zlib1g-dev zlib1g:i386

sudo apt-get install -y python3-dev
sudo apt-get install -y autoconf automake libtool
sudo apt-get install -y libltdl-dev
sudo apt-get install -y libffi-dev # ctypes
sudo apt-get install -y xclip # clipboard stuff

# create buildozer.spec
# buildozer init

# run first build (Android target)
# buildozer -v android debug

# note: for XML Schema errors:
# --add-modules java.xml.bind
# DEFAULT_JVM_OPTS='"-Dcom.android.sdklib.toolsdir=$APP_HOME" --add-modules java.xml.bind'

# note; jnius/jnius.c not found
# pip install -U cython (0.29.5)
# pyjnius will build once and can switch back to cython==0.21

# Install necessary system packages
sudo apt-get install -y \
    python-pip \
    build-essential \
    git \
    python \
    python-dev \
    python3-dev \
    ffmpeg \
    libsdl2-dev \
    libsdl2-image-dev \
    libsdl2-mixer-dev \
    libsdl2-ttf-dev \
    libportmidi-dev \
    libswscale-dev \
    libavformat-dev \
    libavcodec-dev \
    zlib1g-dev

## Install gstreamer for audio, video (optional)
#sudo apt-get install -y \
#    libgstreamer1.0 \
#    gstreamer1.0-plugins-base \
#    gstreamer1.0-plugins-good
