#!/bin/bash

# Download Java JARS
DIR="$HOME/.buildozer/android/platform/android-sdk/tools/lib/jaxb_lib"

mkdir -p ${DIR}

wget http://central.maven.org/maven2/javax/activation/activation/1.1.1/activation-1.1.1.jar -O ${DIR}/activation.jar
wget http://central.maven.org/maven2/javax/xml/jaxb-impl/2.1/jaxb-impl-2.1.jar -O ${DIR}/jaxb-impl.jar
wget http://central.maven.org/maven2/org/glassfish/jaxb/jaxb-xjc/2.3.2/jaxb-xjc-2.3.2.jar -O ${DIR}/jaxb-xjc.jar
wget http://central.maven.org/maven2/org/glassfish/jaxb/jaxb-core/2.3.0.1/jaxb-core-2.3.0.1.jar -O ${DIR}/jaxb-core.jar
wget http://central.maven.org/maven2/org/glassfish/jaxb/jaxb-jxc/2.3.2/jaxb-jxc-2.3.2.jar -O ${DIR}/jaxb-jxc.jar
wget http://central.maven.org/maven2/javax/xml/bind/jaxb-api/2.3.1/jaxb-api-2.3.1.jar -O ${DIR}/jaxb-api.jar

# put on CLASSPATH for sdkmanager, avdmanager
CP='CLASSPATH=$CLASSPATH:$APP_HOME/lib/jaxb_lib/activation.jar:$APP_HOME/lib/jaxb_lib/jaxb-impl.jar:$APP_HOME/lib/jaxb_lib/jaxb-xjc.jar:$APP_HOME/lib/jaxb_lib/jaxb-core.jar:$APP_HOME/lib/jaxb_lib/jaxb-jxc.jar:$APP_HOME/lib/jaxb_lib/jaxb-api.jar'

#echo ${CP} >> ~/.buildozer/android/platform/android-sdk/tools/bin/sdkmanager
#echo ${CP} >> ~/.buildozer/android/platform/android-sdk/tools/bin/avdmanager

# place immediately under the CLASSPATH declarations in sdkmanager, avdmanager
sed -i "/^CLASSPATH=.*/a ${CP}" ~/.buildozer/android/platform/android-sdk/tools/bin/sdkmanager
sed -i "/^CLASSPATH=.*/a ${CP}" ~/.buildozer/android/platform/android-sdk/tools/bin/avdmanager
