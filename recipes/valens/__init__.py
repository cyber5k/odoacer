from pythonforandroid.recipe import CythonRecipe

class ValensRecipe(CythonRecipe):
    """
    Python-for-android recipe for Valens: a python wrapper for a C implementation of NTRU
    """

    version = 'stable'
    url = 'https://bitbucket.org/safoerster/public/downloads/valens-0.1.tar.gz'
    name = 'valens'

    depends = ['setuptools','cython']

recipe = ValensRecipe()
