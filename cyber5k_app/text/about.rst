
.. _top:

Cyber5K
=======

Cyber5K is a platform for demonstrating security and interesting projects. Deriving from the standard race distance for beginners, Cyber5K is meant to highlight beginning or entry moves into diverse cyber arenas.

I am passionate about communications and security. In 2014 a fellow engineer and I released a full end-to-end encryption platform called TippingTrees. Credentials were RSA-2048 keys and the backend was designed so that even dumps of the database dumps would not reveal who was communicating with whom (a design we called Zero-Knowledge).

All modern public key cryptosystems are vulnerable to quantum computing algorithms, namely Shor's and Grover's algorithms. While international standards organizations, cryptographers, and mathematicians around the world hash out new standards believed to be quantum-resistant, there are no such quantum-resistant algorithms in common use in 2019.

Cyber5K brings a frontrunner quantum-resistant public key cryptosystem called NTRU to you. NTRU is a lattice-based system that can be used for encryption, signing, and even fully homomorphic encryption. Cyber5K starts by demonstrating NTRU encryption and perfect forward secrecy.

There are many end-to-end encryption platforms available today. But end-to-end encryption is only as good as the encryption itself. All commonly used platforms today use quantum-vulnerable encryption. This means that if your enc-to-end encrypted communications are recorded today or collected in the future, as soon as an entity has a sufficient quantum computer all your communications are vulnerable to third-party decryption. NTRU is quantum-resistent. NTRU has been around for decades and there is no known theoretical attack against NTRU even for future quantum computers.

Steven Foerster
===============
    
steven@cyber5k.com

* Master of Science in Computer Science, Georgia Institute of Technology
* Bachelor of Science in Electrical Engineering, Brigham Young University

Amateur Radio Callsign: KW4TD

First Marathon: January 2018

Stilicho
========
Stilicho is the umbrella name for three Cyber5K integrated projects:

* **Odoacer**
    Kivy cross-platform application (this app that you are using) enabling registration with Trajan backend and management of personas (encrypted credentials) for quantum-resistant (NTRU) end-to-end encrypted communications via websockets.
* **Valens**
    Python wrapper for C implementation of NTRUencrypt. Valens can generate keys, export keys, import keys, encrypt, and decrypt. By default the EES1087EP2 parameters are used. Valens is installed on Odoacer for client-side key generation and encryption. Valens has a pythonforandroid (p4a) recipe for integration with Kivy apps.
* **Trajan**
    A Django 2 backend functioning as both the landing page for https://cyber5k.com and the Channels websocket server used by Odoacer.

Selected Other Projects
=======================
* Tipping Trees
* Beowulf cluster
* MATLAB ANN (artificial neural network) image processing tools


Third party projects contributed to:
====================================
The following projects have merged pull requests:

* libntru: C implementation of NTRU
    https://github.com/tbuktu/libntru/

* passporteye: Google Tesseract based computer vision tool
    https://github.com/konstantint/PassportEye/
