import Crypto
from Crypto.PublicKey import RSA
from Crypto import Random
import ast

random_generator = Random.new().read
key = RSA.generate(4096, random_generator) #generate pub and priv key

publickey = key.publickey() # pub key export for exchange

# encrypt
encrypted = publickey.encrypt('encrypt this message'.encode('utf-8'), 32)
print('encrypted message:', encrypted[0]) #ciphertext

#decrypt
decrypted = key.decrypt(ast.literal_eval(str(encrypted[0])))
print('decrypted', decrypted)

# sign
sig = key.sign('msg'.encode('utf-8'), 10)
print('sig: {}'.format(sig))

# verify
print('verify: {}'.format(key.verify('msg'.encode('utf-8'), sig)))
print('verify: {}'.format(key.verify('notmsg'.encode('utf-8'), sig)))

# export public
print('public key: {}'.format(publickey.exportKey('DER')))
print("")

# export key
print('key: {}'.format(key.exportKey('DER')))

print(len(key.exportKey('DER')))

#######

key = RSA.generate(2048, random_generator)

binPrivKey = key.exportKey('DER')
binPubKey =  key.publickey().exportKey('DER')

privKeyObj = RSA.importKey(binPrivKey)
pubKeyObj =  RSA.importKey(binPubKey)

msg = "attack at dawn"
emsg = pubKeyObj.encrypt(msg.encode('utf-8'), 'x')[0]
print('emsg: {}'.format(emsg))

dmsg = privKeyObj.decrypt(emsg)
#dmsg = privKeyObj.decrypt(ast.literal_eval(str(emsg)))
print('dmsg: {}'.format(dmsg))

