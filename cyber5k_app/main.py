import kivy
kivy.require('1.10.1')

import asyncio
from datetime import datetime

from kivy.app import App
from kivy.lang import Builder
from kivy.config import Config
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import NumericProperty, StringProperty, BooleanProperty, ListProperty, ObjectProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.core.text import Label as CoreLabel
from kivy.uix.button import Button
from kivy.uix.bubble import Bubble
from kivy.uix.progressbar import ProgressBar
from kivy.uix.modalview import ModalView
from kivy.uix.filechooser import FileChooserIconView, FileChooserListView
from kivy.uix.scatter import Scatter
from kivy.core.window import Window
from kivy.graphics import Color, RoundedRectangle, Ellipse, Rectangle
from kivy.graphics.texture import Texture
from kivy.metrics import dp

from kivy.clock import Clock
from kivy.core.clipboard import Clipboard

from plyer import notification
#from jnius import autoclass

from functools import partial
import websocket
import threading
import os
import json
import string
import random
import webbrowser
from requests import Session
from requests.exceptions import ConnectionError
from websocket._exceptions import WebSocketConnectionClosedException

from pong import PongGame, PongPaddle, PongBall
import valens

import hashlib
import base64
import Crypto
from Crypto.PublicKey import RSA
from Crypto import Random
import ast
from util.aes import AESCipher


Config.set('kivy','log_level','debug')
PORT = 8000 
PROTO = 'http'
DEBUG = True

if DEBUG:
    print('DEBUG MODE ENABLED')

def gen_rand_string(length):
    c = ['a','b','c','d','e','f','g','h','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z']
    c += ['A','B','C','D','E','F','G','H','J','K','M','N','P','Q','R','S','T','U','V','W','X','Y','Z']
    c += ['2','3','4','5','6','7','8','9']
    return ''.join(random.choices(c, k=length))

def esc_markup(msg):
    return (msg.replace('&', '&amp;')
            .replace('[', '&bl;')
            .replace(']', '&br;'))

class ActionButton(Button):
    inside_group = ObjectProperty()
    icon = StringProperty()

class ChatLabelSelf(Label):
    pass

class ChatLabelOther(Label):
    pass

class Picture(Scatter):
    """
    The source property will be the filename to show.
    """

    source = StringProperty(None)

class CircularProgressBar(ProgressBar):

    def __init__(self, **kwargs):
        super(CircularProgressBar, self).__init__(**kwargs)

        # Set constant for the bar thickness
        self.thickness = 40

        # Create a direct text representation
        self.label = CoreLabel(text="0%", 
                               font_size=self.thickness)

        # Initialise the texture_size variable
        self.texture_size = None

        # Refresh the text
        self.refresh_text()

        # Redraw on innit
        self.draw()

    def draw(self):

        with self.canvas:

            # Empty canvas instructions
            self.canvas.clear()

            # Draw no-progress circle
            Color(0.26, 0.26, 0.26)
            Ellipse(pos=self.pos, size=self.size)

            # Draw progress circle, small hack if there is no progress (angle_end = 0 results in full progress)
            Color(1, 0, 0)
            Ellipse(pos=self.pos, size=self.size,
                    angle_end=(0.001 if self.value_normalized == 0 else self.value_normalized*360))

            # Draw the inner circle (colour should be equal to the background)
            Color(0, 0, 0)
            Ellipse(pos=(self.pos[0] + self.thickness / 2, self.pos[1] + self.thickness / 2),
                    size=(self.size[0] - self.thickness, self.size[1] - self.thickness))

            # Center and draw the progress text
            Color(1, 1, 1, 1)
            Rectangle(texture=self.label.texture, size=self.texture_size,
                      pos=(self.size[0]/2 - self.texture_size[0]/2, self.size[1]/2 - self.texture_size[1]/2))

    def refresh_text(self):
        # Render the label
        self.label.refresh()

        # Set the texture size each refresh
        self.texture_size = list(self.label.texture.size)

    def set_value(self, value):
        # Update the progress bar value
        self.value = value

        # Update textual value and refresh the texture
        self.label.text = str(int(self.value_normalized*100)) + "%"
        self.refresh_text()

        # Draw all the elements
        self.draw()


class RootWidget(BoxLayout): # ScreenManager
    def __init__(self, app, **kwargs):
        super(RootWidget, self).__init__(**kwargs)

        # get list of kv files
        curdir = os.path.dirname(__file__)
        app.kvpath = os.path.join(curdir, 'kv')
        #files = [f for f in os.listdir(kvpath) if os.path.isfile(os.path.join(kvpath, f))]

        # load kv files
        #for f in files:
        #    self.ids.sm.add_widget(Builder.load_file(os.path.join(kvpath, f)))
        for f in app.screen_names+app.screen_names_nospin:
            self.ids.sm.add_widget(Builder.load_file(os.path.join(app.kvpath, f+'.kv')))
            
        
class Cyber5KScreen(Screen):
    fullscreen = BooleanProperty(True)

    def add_widget(self, *args):
        if 'content' in self.ids:
            return self.ids.content.add_widget(*args)
        return super(Cyber5KScreen, self).add_widget(*args)

class Cyber5KApp(App):

    current_title = StringProperty()
    time = NumericProperty(0)
    hierarchy = ListProperty([])
    
    def build(self):

        # initialize 
        self.title = 'Cyber5K by Steven Foerster'

        # read settings
        self.base_folder =os.path.dirname(os.path.abspath('.'))
        self.setting_file = os.path.join(self.base_folder, 'cyber5k_setting.json')
        self.read_config()

        # bind keyboard
        Window.bind(on_keyboard=self.key_input)
        
        self.screen_names = [
            'about',
            'demo',
            'pong',
        ]

        self.screen_names_nospin = [
            'login',
            'register',
        ]
        
        root = RootWidget(self)
        
        # set first screen
        root.ids.sm.current = 'login'
        
        # initialize pong
        self.pong_game = PongGame()
        self.pong_game.reset()
        root.ids.sm.get_screen('pong').add_widget(self.pong_game)
        
        return root

    def key_input(self, window, key, scancode, codepoint, modifier):
      if key == 27:   # back button
         return True  # override the default behaviour
      else:           # the key now does nothing
         return False
    
    def read_config(self):
        try:
            with open(self.setting_file, 'r') as f:
                text = f.read()
            self.setting_dict = json.loads(text)

            self.host = self.setting_dict['host']
            self.username = self.setting_dict['username']
        except:
            self.host = "127.0.0.1"
            self.username = "kivy"
        
        self.password = ''

    def save_config(self):
        self.setting_dict = {'host': self.host, 'username': self.username}
        with open(self.setting_file, 'w') as f:
            f.write(json.dumps(self.setting_dict))
    
    def on_pause(self):
        # save data if needed
        return True

    def on_resume(self):
        pass

    def register(self):
        self.root.ids.sm.current = 'login' 
        
    def _getCSRFtoken(self, URL=None):

        if not URL:
            URL = "{}://{}:{}/accounts/login/".format(PROTO, self.host, PORT)

        if not hasattr(self, 'session'):
            self.session = Session()
        
        try:
            request_response = self.session.get(URL)
        except ConnectionError:
            self._popup('Server is unreachable')
            return

        if request_response.status_code != 200:
            self._popup('Unable to connect')
            return

        if 'csrftoken' in self.session.cookies:
            csrftoken = self.session.cookies['csrftoken']
        else:
            self._popup('No CSRF token found')
            return

        return csrftoken
    
    def login(self):
       
        # initialize
        self.host = self.root.ids.sm.get_screen('login').ids.server.text
        self.username = self.root.ids.sm.get_screen('login').ids.username.text
       
        # get the CSRF token
        URL = "{}://{}:{}/accounts/login/".format(PROTO, self.host, PORT)
        if not self._getCSRFtoken(URL):
            return

        # login
        URL = "{}://{}:{}/apilogin/".format(PROTO, self.host, PORT)
        login_response = self.session.post(URL,
                                           data = {
                                                'username': self.username,
                                                'password': self.root.ids.sm.get_screen('login').ids.password.text,
                                                'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                           },
                                           headers = {
                                            'Referer': URL,
                                           })

        # if login was successful, open up the websockets
        if login_response.status_code == 200:
            
            # hash password and store, then clear password
            self.encpw = hashlib.sha256(self.root.ids.sm.get_screen('login').ids.password.text.encode() + 'Cyber5K salt brought to you by Steven Foerster'.encode()).hexdigest()
            self.root.ids.sm.get_screen('login').ids.password.text = ''

            # setup user interface
            self._initUser()

    def logout(self, *args):

        # logout
        URL = "{}://{}:{}/apilogout/".format(PROTO, self.host, PORT)

        try:
            logout_response = self.session.post(URL,
                                               data = {
                                                    'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                               },
                                               headers = {
                                                'Referer': URL,
                                               })
        except ConnectionError:
            print('Unable to connect. Cleaning up.')

        # if logout was successful, cleanup
        #if logout_response.status_code == 200:
            
        # close websocket
        self.ws.close()
        
        # clean saved hash
        self.encpw = '' 
    
        # remove user screen and return to loggedout widget
        if self.root.ids.sm.has_screen('user'):
            self.root.ids.sm.remove_widget(self.root.ids.sm.get_screen('user'))

        # close chatrooms
        for pk in self.rooms:
            self.root.ids.sm.remove_widget(self.root.ids.sm.get_screen('chatroom-{}'.format(self.rooms[pk])))

        # remove room screen
        self.root.ids.sm.remove_widget(self.root.ids.sm.get_screen('rooms'))
        
        # clean all room data
        self.rooms = {}
        self.room_widgets = {}
        self.room_info = []
        self.roomkeys = {}

        # clean all persona data
        self.persona_widgets = {}
        self.persona_info = []

        # remove user spinner
        self.root.ids.av.remove_widget(self.user_spnr)

        # add login screen
        self.loggedout_spinner = Builder.load_file(os.path.join(self.kvpath, 'logged_out.kv'))
        self.root.ids.av.add_widget(self.loggedout_spinner)

        # set screen
        self.go_screen_name('login')
            
    def go_screen(self, idx):
        self.root.ids.sm.current = self.screen_names[idx]
    
    def go_screen_name(self, name, *args):
        if DEBUG:
            print('name: {}'.format(name))
        #print('args: {}'.format(args))
        self.root.ids.sm.current = name
    
    def demo_encrypt(self):
        
        self.root.ids.sm.get_screen('demo').ids.demo_button_encrypt.disabled = True
        
        self.demoV = valens.Valens()
        try:
            self.demoV.importPubKey(self.root.ids.sm.get_screen('demo').ids.demo_pubkey.text)
        except Exception as e:
            self.root.ids.sm.get_screen('demo').ids.demo_pubkey.text = str(e)
            return

        try:
            self.root.ids.sm.get_screen('demo').ids.demo_ciphertext.text = self.demoV.encryptMsg(self.root.ids.sm.get_screen('demo').ids.demo_plaintext.text)
        except Exception as e:
            self.root.ids.sm.get_screen('demo').ids.demo_plaintext.text = str(e)
            
        self.root.ids.sm.get_screen('demo').ids.demo_plaintext.text = '' 
        self.root.ids.sm.get_screen('demo').ids.demo_button_encrypt.disabled = False

    def demo_decrypt(self):

        self.root.ids.sm.get_screen('demo').ids.demo_button_decrypt.disabled = True
        
        self.demoV = valens.Valens()
        try:
            self.demoV.importPubKey(self.root.ids.sm.get_screen('demo').ids.demo_pubkey.text)
            self.demoV.importPrivKey(self.root.ids.sm.get_screen('demo').ids.demo_privkey.text)
        except Exception as e:
            self.root.ids.sm.get_screen('demo').ids.demo_privkey.text = str(e)
            return

        try:
            self.root.ids.sm.get_screen('demo').ids.demo_plaintext.text = self.demoV.decryptMsg(self.root.ids.sm.get_screen('demo').ids.demo_ciphertext.text.strip())
        except Exception as e:
            self.root.ids.sm.get_screen('demo').ids.demo_ciphertext.text = str(e)
            return

        self.root.ids.sm.get_screen('demo').ids.demo_ciphertext.text = '' 
        self.root.ids.sm.get_screen('demo').ids.demo_button_decrypt.disabled = False

    def demo_initialize(self):
        # initialize demo
        self.demoV = valens.Valens()
        self.root.ids.sm.get_screen('demo').ids.demo_pubkey.text = self.demoV.getPubKey()
        self.root.ids.sm.get_screen('demo').ids.demo_privkey.text = self.demoV.getPrivKey()
        self.root.ids.sm.get_screen('demo').ids.demo_plaintext.text = 'The lazy brown fox jumped quickly over the fence. Using current NTRU parameters only the first {} characters can be encrypted.'.format(self.demoV.get_max_msg_len())
        self.root.ids.sm.get_screen('demo').ids.demo_ciphertext.text = ''
        
        self.root.ids.sm.get_screen('demo').ids.demo_button_encrypt.disabled = False
        self.root.ids.sm.get_screen('demo').ids.demo_button_decrypt.disabled = False

    def demo_reset(self):

        self.root.ids.sm.get_screen('demo').ids.demo_button_encrypt.disabled = True
        self.root.ids.sm.get_screen('demo').ids.demo_button_decrypt.disabled = True

        self.root.ids.sm.get_screen('demo').ids.demo_pubkey.text = 'NTRU public key'
        self.root.ids.sm.get_screen('demo').ids.demo_privkey.text = 'NTRU private key'
        self.root.ids.sm.get_screen('demo').ids.demo_plaintext.text = 'Plaintext'
        self.root.ids.sm.get_screen('demo').ids.demo_ciphertext.text = 'Ciphertext'

    def pong_toggle(self):
        
        if hasattr(self, 'pong_event') and self.pong_event:
            Clock.unschedule(self.pong_event)
            self.pong_event = None
        else:
            self.pong_event = Clock.schedule_interval(self.pong_game.update, 1.0 / 60.0)

    def _on_open(self):
        if DEBUG:
            print("Websocket opened.")
        #self._add_msg('Websocket connected.')
        #self.transport = transport
        
        # join rooms
        for pk in self.rooms:
            self._join_room(self.rooms[pk])

    def _join_room(self, room):

        self.ws.send(
            json.dumps({
                'command': 'join',
                'room': room,
            })
        )
    
    def _leave_room(self, room, *args):

        self.ws.send(
            json.dumps({
                'command': 'leave',
                'room': room,
            })
        )
    
    def _on_message(self, data):

        if not data:
            print('Empty data.')
            return
        
        #text = data.decode('utf-8', 'ignore')
        text = json.loads(data)

        # Initialize
        if DEBUG:
            print('text:')
            for key in text:
                print('    {}: {}'.format(key, text[key]))
        
        username = ''
        msg = ''
        room = ''

        if 'room' in text:
            room = text['room']

        # Begin message handling
        # ERROR
        #if hasattr(text, 'error'):
        if 'error' in text:
            self._add_msg('ERROR: {}'.format(text.error), room)
            return

        # JOIN
        #elif hasattr(text, 'join'):
        elif 'join' in text:
            #self._add_msg('JOIN: not implemented yet')
            print('Joining room: {}'.format(text['title']))

            chatroom = Builder.load_file(os.path.join(self.kvpath, 'chatroom.kv'))
            chatroom.name = 'chatroom-{}'.format(text['title'])
            chatroom.ids.message.bind(on_text_validate=partial(self.send_msg, text['title']))
            chatroom.ids.send_btn.bind(on_press=partial(self.send_msg, text['title']))
            chatroom.ids.add_file.bind(on_press=partial(self._show_load_file, text['title']))
            chatroom.ids.chat_logs.bind(minimum_height=chatroom.ids.chat_logs.setter('height'))
            self.root.ids.sm.add_widget(chatroom)
            return

        # LEAVE
        #elif hasattr(text, 'leave'):
        elif 'leave' in text:
            #self._add_msg('LEAVE: not implemented yet', room)
            print('Leaving room {}'.format(text['leave']))
            pk = self._getRoom_pkfromGroup(text['leave'])
            self._deleteRoom(pk)
            return

        # MSG
        #elif 'message' in text: 
        if 'msg_type' not in text:
            self._add_msg('Unrecognized msg type.', room)
            return
        
        msg_type = int(text['msg_type'])

        # notification
        if 'username' in text and 'message' in text:
            nmsg = '{}: {}'.format(text['username'], text['message'])
        elif 'message' in text:
            nmsg = text['message']
        else:
            nmsg = text

        #notification.notify(
        #    title='Odoacer: {}'.format(room),
        #    message=nmsg)
        
        if msg_type == 0:
            # message
            username = 'unknown'
            if 'username' in text:
                username = text['username']
            self._add_msg(text['message'], room, username, replace=self._roomDecrypt(text['message'], room))
            return
        
        elif msg_type == 1:
            # warning /advice messages
            self._add_msg('Warning: {}'.format(text['message']), room)
            return

        elif msg_type == 2:
            # alert / danger messages
            self._add_msg('Alert: {}'.format(text['message']), room)
            return

        elif msg_type == 3:
            # muted message
            self._add_msg('Muted: {}'.format(text['message']), room)
            return

        elif msg_type == 4:
            # user joined room
            username = 'unknown'
            if 'username' in text:
                username = text['username']
            self._add_msg('User joined: {}'.format(username), room)
            return

        elif msg_type == 5:
            # user left room
            username = 'unknown'
            if 'username' in text:
                username = text['username']
            self._add_msg('User left: {}'.format(username), room)
            return

        elif msg_type == 6:
            # system notification
            if 'msg' in text:
                self._notification_handler(text['msg'])
                return
            print('System message without msg!!!')

        elif msg_type == 7:
            # user sent file
            username = 'unknown'
            if 'username' in text:
                username = text['username']

            data_dir = getattr(self, 'user_data_dir')
           
            dec_file = self._roomDecrypt(text['message'].encode('utf-8'), room)
            decoded_file = base64.b64decode(dec_file)

            fname = self._roomDecrypt(text['fname'].encode('utf-8'), room)
            filepath = os.path.join(data_dir, fname)
            print("Writing file: {}".format(filepath))
            with open(filepath, "wb") as fp:
                #fp.write(dec_file.decode('base64'))
                fp.write(decoded_file)
            
            self._add_msg('FILE> {}'.format(fname), room, username, link='file://{}'.format(filepath))

            return

        #self._add_msg('Unsupported message type')
        #self._add_msg('Unknown data: {}'.format(text))
        
        if DEBUG:
            print('Unknown data: {}'.format(text))

        #else:
        #    self._add_msg('Unknown data: {}'.format(text))
        
        #msg = self.app.V.decryptMsg(msg)
        #print("Decryption: {}".format(msg))
            
    def _lbl_replacetxt(self, lbl, replace, *args):
        lbl.text = replace
    
    def _msg_str(self, msg, username=None, link=None):
        
        if username:
            if username.lower() == self.username.lower():
                color = 'ffffff'
            else:
                color = '000000'

            # put it 'ref' tags if link exists
            if link:
                txt = '[b][color={}]{}:[/color][/b] [ref=link][color={}]{}[/color][/ref]\n'.format(color, username, color, esc_markup(msg))
            else:
                txt = '[b][color={}]{}:[/color][/b] [color={}]{}[/color]\n'.format(color, username, color, esc_markup(msg))
        
        else:
            txt = '[b][color=7950e9]{}[/color][/b]\n'.format(esc_markup(msg))

        return txt
    
    def _add_msg(self, msg, room, username=None, replace=None, link=None):

        msg = str(msg)
        
        txt = self._msg_str(msg, username, link)
        
        #self.root.ids.sm.get_screen('chatroom-{}'.format(room)).ids.chat_logs.text += (
        #    txt
        #)

        if username and username.lower() == self.username.lower():
            lbl = ChatLabelSelf(text=txt)
        else:
            lbl = ChatLabelOther(text=txt)
        
        lbl.bind(width=lambda s, w: s.setter('text_size')(s, (w, None)))
        lbl.bind(texture_size=lbl.setter('size'))

        if link:
            lbl.bind(on_ref_press=partial(webbrowser.open, link))

        self.root.ids.sm.get_screen('chatroom-{}'.format(room)).ids.chat_logs.add_widget(lbl)

        if replace:
            Clock.schedule_once(partial(self._lbl_replacetxt, lbl, self._msg_str(replace, username)), 3)
        
        # scroll to bottom
        self.root.ids.sm.get_screen('chatroom-{}'.format(room)).ids.chat_logs.scroll_y = 0

        #Clipboard.copy(msg)

    def _on_close(self):
        print('The server closed the connection')
        #self.transport.close()

    def _on_error(self, error):
        if DEBUG:
            print("Websocket error: {}".format(error))
    
    def connect(self):
        
        self.host = self.root.ids.sm.get_screen('login').ids.server.text
        self.username = self.root.ids.sm.get_screen('login').ids.username.text

        self.is_stop = False
        self.loop = asyncio.get_event_loop()

        #if self.reconnect():

        #self.clock_receive = Clock.schedule_interval(self.receive_msg, 1)
        #self.clock_detect = Clock.schedule_interval(self.detect_if_offline, 3)

        #self.root.ids.sm.current = 'chatroom' 
        self.save_config()
        if DEBUG:
            print('-- connecting to ' + self.host)
            websocket.enableTrace(True)        
        else:
            websocket.enableTrace(False)        

        self.ws = websocket.WebSocketApp('ws://{}:{}/chat/stream/'.format(self.host, PORT),
                                         on_open = self._on_open,
                                         on_message = self._on_message,
                                         on_close = self._on_close,
                                         on_error = self._on_error,
                                         cookie = self._getCookies(self.host))

        #self.ws.run_forever()
        wst = threading.Thread(target=self.ws.run_forever)
        wst.daemon = True
        wst.start()

    #def reconnect(self):
    #    try:
    #        self.coro = self.loop.create_connection(lambda: ClientProtocol(self, self.loop),
    #                      self.host, PORT)
    #        self.transport, self.protocol = self.loop.run_until_complete(self.coro)

    #        self.last_connection_time = datetime.now()
    #        print("I just reconnected the server.")
    #        return True
    #    except Exception as e:
    #        #print(e)
    #        self.root.current = 'login'
    #        try:
    #            self.clock_receive.cancel()
    #            self.clock_detect.cancel()
    #        except:
    #            print("No server available.")
    #        return False

    #def detect_if_offline(self, dt): #run every 3 seconds
    #    if (datetime.now() - self.last_connection_time).total_seconds() > 45:
    #        self.transport.close()
    #        self.reconnect()

    def send_msg(self, room, *args):
        #if self.transport.is_closing():
        #    self.transport.close()
        #    self.reconnect()
        
        msg = self.root.ids.sm.get_screen('chatroom-{}'.format(room)).ids.message.text
        #emsg = self.V.encryptMsg(self.root.ids.message.text)
        
        #self.transport.write('{0}:{1}'.format(self.nick, msg).encode('utf-8', 'ignore'))
        #self.ws.send('{0}:{1}'.format(self.nick, msg))
        emsg = self._roomEncrypt(msg, room).decode('utf-8')
       
        try:
            self.ws.send(
                json.dumps({
                    'command': 'send',
                    'room': room,
                    'message': emsg,
                })
            )
        except WebSocketConnectionClosedException:
            self._popup('Connection lost')
            self.logout()
            return

        #self.root.ids.chat_logs.text += (
        #    '[b][color=2980b9]{}:[/color][/b] {}\n'
        #        .format(self.nick, esc_markup(msg)))
        
        #self._add_msg(msg, self.username)
        self.root.ids.sm.get_screen('chatroom-{}'.format(room)).ids.message.text = ''

    #def receive_msg(self, dt):
    #    self.loop.run_until_complete(self.coro)

    #### PERSONA MANAGEMENT ####
    def _getPersona_name2pk(self, name):
        
        for persona in self.persona_info:
            if persona['name'] == name:
                return persona['id']

        return None
    
    def _getPersona_from_name(self, name):
        
        for persona in self.persona_info:
            if persona['name'] == name:
                return persona

        return None

    def _getPersona_ekp(self, ekp_id):
        
        for persona in self.persona_info:
            if persona['enckeypair'][0]['id'] == int(ekp_id):
                return persona['enckeypair'][0], persona

        return None
    
    def _getPersonas(self):
        URL = "{}://{}:{}/api/personas/".format(PROTO, self.host, PORT)
        response = self.session.get(URL,
                                     data = {
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     headers = {
                                      'Referer': URL,
                                     })
        
        # personas
        #self._popup(str(response.content))
        if response.status_code != 200:
            self._popup('Unable to load personas')
            return
        
        # persona screen
        
        # remove if exists
        if not self.root.ids.sm.has_screen('personas'):
            # build
            self.root.ids.sm.add_widget(Builder.load_file(os.path.join(self.kvpath, 'personas.kv')))
        
        self.root.ids.sm.get_screen('personas').ids.persona_scroll.clear_widgets()
        self.root.ids.sm.get_screen('personas').ids.persona_scroll.bind(minimum_height=self.root.ids.sm.get_screen('personas').ids.persona_scroll.setter('height'))

        self.persona_widgets = {}
        self.persona_info = []
        # add persona buttons
        personas = json.loads(response.content)
        for persona_obj in personas['results']:
            persona = persona_obj['name']
            self.persona_info.append(persona_obj)
            
            # test to see if you can decrypt, delete persona if you can't
            try:
                aes = AESCipher(self.encpw)
                aes.decrypt(persona_obj['enckeypair'][0]['encprivkey'])
            except UnicodeDecodeError:
                if DEBUG:
                    print('Password changed? Encryption keys no longer valid.')
                self._deletePersona(persona_obj['id'])
                continue

            box = BoxLayout(id='persona_{}'.format(persona_obj['id']),
                            orientation='horizontal',
                            height=dp(90),
                            size_hint_y=None)
            
            box.add_widget(Button(text=persona,
                                  size_hint_y=None,
                                  height=dp(90)))
            
            box.add_widget(Label(text='',
                                 size_hint=(1,None),
                                 height=dp(90)))
                                 #size=(dp(100),dp(100))))

            box.add_widget(Button(text='Delete',
                                  size_hint_y=None,
                                  height=dp(90),
                                  on_press=partial(self._deletePersona, persona_obj['id'])))

            self.persona_widgets[persona_obj['id']] = box
            self.root.ids.sm.get_screen('personas').ids.persona_scroll.add_widget(box)
        
        # if room_add screen exists then update personas
        if self.root.ids.sm.has_screen('room_add'):
            self.root.ids.sm.get_screen('room_add').ids.room_add_spinner.values = [x['name'] for x in self.persona_info]


    def _deletePersona(self, pk, *args):
        
        URL = "{}://{}:{}/api/personas/{}".format(PROTO, self.host, PORT, pk)
        csrftoken = self._getCSRFtoken(URL)
        if not csrftoken:
            self.logout()
            return

        response = self.session.delete(URL,
                                     data = {
                                          'pk': pk,
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     headers = {
                                      'Referer': URL,
                                      'X-CSRFToken': csrftoken,
                                     })
        if response.status_code == 204:
            # remove widget from personas
            if pk in self.persona_widgets:
                self.root.ids.sm.get_screen('personas').ids.persona_scroll.remove_widget(self.persona_widgets[pk])
            
            # remove from self.persona_info
            self.persona_info = [x for x in self.persona_info if x['id'] != pk]
        
            # if room_add screen exists then update personas
            if self.root.ids.sm.has_screen('room_add'):
                self.root.ids.sm.get_screen('room_add').ids.room_add_spinner.values = [x['name'] for x in self.persona_info]
    
    def _createPersonaWrapper(self):
        
        # Progress Bar
        self.root.disabled = True
        mv = ModalView(size_hint=(None, None), 
                          size=(dp(400), dp(400)))
        pgbar = CircularProgressBar(size_hint=(None,None),
                                    height=dp(200),
                                    width=dp(200),
                                    max=80)
        mv.add_widget(pgbar)
        mv.open()
        
        pgbar.set_value(0)

        tpersona = threading.Thread(target=self._createPersona, args=(mv,pgbar))
        tpersona.start()
    
    def _createPersona(self, mv, pgbar):
        # AES cipher object
        aes = AESCipher(self.encpw)
        pgbar.set_value(5)
        
        # generate enckeypair
        V = valens.Valens()
        pgbar.set_value(40)
        ekp_pub = V.getPubKey()
        ekp_encpriv = aes.encrypt(V.getPrivKey())
        pgbar.set_value(45)

        # generate signkeypair
        random_generator = Random.new().read
        
        key = RSA.generate(2048, random_generator) #generate pub and priv key
        pgbar.set_value(55)
        skp_pub = base64.b64encode(key.publickey().exportKey('DER')) # pub key export for exchange
        skp_encpriv = aes.encrypt(str(key.exportKey('DER')))
        pgbar.set_value(75)
        
        # create persona
        URL = "{}://{}:{}/api/personas/".format(PROTO, self.host, PORT)
        csrftoken = self._getCSRFtoken(URL)
        if not csrftoken:
            self.logout()
            return

        response = self.session.post(URL,
                                     data = {
                                          'name': gen_rand_string(6),
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     files={},
                                     headers = {
                                      'Referer': URL,
                                      'X-CSRFToken': csrftoken,
                                     })
        
        persona_data = json.loads(response.content)
        
        if response.status_code == 201:
            # persona created
            
            # populate EncKeyPair
            URL = "{}://{}:{}/api/enckeypair/".format(PROTO, self.host, PORT)
            csrftoken = self._getCSRFtoken(URL)
            eresponse = self.session.post(URL,
                                     data = {
                                          'persona': persona_data['id'],
                                          'name': 'NTRU-256-default-1',
                                          'pubkey': ekp_pub,
                                          'encprivkey': ekp_encpriv,
                                          'privkey_enctype': 'AES-256-CBC-1',
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     files={},
                                     headers = {
                                      'Referer': URL,
                                      'X-CSRFToken': csrftoken,
                                     })

            # populate SignKeyPair
            URL = "{}://{}:{}/api/signkeypair/".format(PROTO, self.host, PORT)
            csrftoken = self._getCSRFtoken(URL)
            sresponse = self.session.post(URL,
                                     data = {
                                          'persona': persona_data['id'],
                                          'name': 'RSA-4096',
                                          'pubkey': skp_pub,
                                          'encprivkey': skp_encpriv,
                                          'privkey_enctype': 'AES-256-CBC-1',
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     files={},
                                     headers = {
                                      'Referer': URL,
                                      'X-CSRFToken': csrftoken,
                                     })
            
            # create persona box
            self._getPersonas() 
        
        # Progress Bar
        self.root.disabled = False
        pgbar.set_value(80)
        mv.dismiss()

    ##### Room Management #######
    def _getKeyfromData(self, text):
       
        # look for persona
        URL = "{}://{}:{}/api/public/personas/{}/".format(PROTO, self.host, PORT, text)
        
        try:
            response = self.session.get(URL,
                                         data = {
                                              'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                         },
                                         headers = {
                                          'Referer': URL,
                                         })
        except ConnectionError:
            self._popup('Unable to connect to server')
            self.logout()
            return
        
        if response.status_code == 200:
            data = json.loads(response.content)
            return data['enckeypair']

        # look for email 
        URL = "{}://{}:{}/api/public/email/{}/".format(PROTO, self.host, PORT, text)
        response = self.session.get(URL,
                                     data = {
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     headers = {
                                      'Referer': URL,
                                     })
        
        if response.status_code == 200:
            data = json.loads(response.content)
            if 'enckeypair' in data['persona']:
                return data['persona']['enckeypair']
            
            # account exists but there's no existing persona to use
            self._popup('No personas found')
            return None

        self._popup('{} not found'.format(text))
        return None

    
    def _createRoom(self): 
        # create room
        otherkey = self._getKeyfromData(self.root.ids.sm.get_screen('room_add').ids.room_add_contact.text)
        
        if not otherkey:
            return
        
        #self._popup('Found: {}'.format(otherkey['id']))
       
        ## create Room
        #URL = "{}://{}:{}/api/rooms/".format(PROTO, self.host, PORT)
        #csrftoken = self._getCSRFtoken(URL)
        #response = self.session.post(URL,
        #                             data = {
        #                                  'owner': self._getPersona_name2pk(self.root.ids.sm.get_screen('room_add').ids.room_add_spinner.text),
        #                                  'title': gen_rand_string(32),
        #                                  'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
        #                             },
        #                             files={},
        #                             headers = {
        #                              'Referer': URL,
        #                              'X-CSRFToken': csrftoken,
        #                             })
        #
        #room_data = json.loads(response.content)
        #print("room_data: {}".format(room_data))

        # room key
        key = hashlib.sha256(Random.new().read(64)).hexdigest()
        # group 
        group = gen_rand_string(32)
        # group name
        group_name = self.root.ids.sm.get_screen('room_add').ids.room_add_name.text

        # encrypt (self)
        persona = self._getPersona_from_name(self.root.ids.sm.get_screen('room_add').ids.room_add_spinner.text)
        if not persona:
            self._popup('Invalid persona selected')
            return
        
        selfkey = persona['enckeypair'][0]
        
        if selfkey['name'] == 'NTRU-256-default-1':
            selfkey_V = valens.Valens()
            selfkey_V.importPubKey(selfkey['pubkey'])
            enckey = selfkey_V.encryptMsg(key)

        aes = AESCipher(key)
        encgroup = aes.encrypt(group)
        encname = aes.encrypt(group_name)
        
        # populate keys in RoomKey (self)
        URL = "{}://{}:{}/api/roomkey/".format(PROTO, self.host, PORT)
        csrftoken = self._getCSRFtoken(URL)
        response = self.session.post(URL,
                                     data = {
                                          'ekp': selfkey['id'], 
                                          'enckey': enckey,
                                          'encgroup': encgroup,
                                          'encgroup_enctype': 'AES-256-CBC-1',
                                          'encname': encname,
                                          'encname_enctype': 'AES-256-CBC-1',
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     files={},
                                     headers = {
                                      'Referer': URL,
                                      'X-CSRFToken': csrftoken,
                                     })
        
        if response.status_code != 201:
            self._popup('There was a problem')
            return
        
        room_obj = json.loads(response.content)
        #print("roomkey_data: {}".format(roomkey_data))
    
        # encrypt for other key
        if otherkey['name'] == 'NTRU-256-default-1':
            otherkey_V = valens.Valens()
            otherkey_V.importPubKey(otherkey['pubkey'])
            enckey_other = otherkey_V.encryptMsg(key)

        aes = AESCipher(key)
        encgroup_other = aes.encrypt(group)
        encname_other = aes.encrypt(group_name)
        
        # populate RoomKey for other
        csrftoken = self._getCSRFtoken(URL)
        response = self.session.post(URL,
                                     data = {
                                          'ekp': otherkey['id'], 
                                          'enckey': enckey_other,
                                          'encgroup': encgroup_other,
                                          'encgroup_enctype': 'AES-256-CBC-1',
                                          'encname': encname_other,
                                          'encname_enctype': 'AES-256-CBC-1',
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     files={},
                                     headers = {
                                      'Referer': URL,
                                      'X-CSRFToken': csrftoken,
                                     })
        
        if response.status_code != 201:
            self._popup('There was a problem')
            return
        
        #roomkey_data = json.loads(response.content)
        #print("roomkey_data: {}".format(roomkey_data))

        # add room to window
        self.roomkeys[group] = key
        self._join_room(group)
        self._addRoom_toWindow(room_obj, group, group_name)
        self.go_screen_name('rooms')

    def _addRoom_toWindow(self, room_obj, group, room_name):
        self.room_info.append(room_obj)
        self.rooms[str(room_obj['id'])] = group
        
        box = BoxLayout(id='room_{}'.format(room_obj['id']),
                        height=dp(90),
                        size_hint_y=None)

        box.add_widget(Button(text=room_name,
                              size_hint_y=None,
                              height=dp(90),
                              on_press=partial(self.go_screen_name, 'chatroom-{}'.format(group))))
        
        box.add_widget(Label(text='',
                             size_hint=(1,None),
                             height=dp(90),
                             size=(dp(100),dp(100))))
        
        box.add_widget(Button(text='Delete',
                              size_hint_y=None,
                              height=dp(90),
                              #on_press=partial(self._deleteRoom, room_obj['id'])))
                              on_press=partial(self._leave_room, group)))

        self.room_widgets[room_obj['id']] = box
        self.root.ids.sm.get_screen('rooms').ids.room_scroll.add_widget(box)

    def _deleteRoom(self, pk, *args):
        URL = "{}://{}:{}/api/roomkey/{}/".format(PROTO, self.host, PORT, pk)
        csrftoken = self._getCSRFtoken(URL)
        response = self.session.delete(URL,
                                     data = {
                                          'pk': pk,
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     headers = {
                                      'Referer': URL,
                                      'X-CSRFToken': csrftoken,
                                     })
        if response.status_code == 204:
            # remove widget from rooms
            if pk in self.room_widgets:
                self.root.ids.sm.get_screen('rooms').ids.room_scroll.remove_widget(self.room_widgets[pk])
            
            # remove from self.room_info
            self.room_info = [x for x in self.room_info if x['id'] != pk]

    def _getRoom_pkfromGroup(self, group):
        for pk in self.rooms:
            if self.rooms[pk] == group:
                return int(pk)

        return None

    def _getRooms(self, roomkey_pk=None):
        
        if roomkey_pk:
            URL = "{}://{}:{}/api/roomkey/{}/".format(PROTO, self.host, PORT, roomkey_pk)
        else:
            URL = "{}://{}:{}/api/roomkey/".format(PROTO, self.host, PORT)
        
        response = self.session.get(URL,
                                     data = {
                                          'csrfmiddlewaretoken': self.session.cookies['csrftoken'],
                                     },
                                     headers = {
                                      'Referer': URL,
                                     })
        
        # rooms
        #self._popup(str(response.content))
        if response.status_code != 200:
            self._popup('Unable to load rooms')
            return
        
        # room screen
        if not self.root.ids.sm.has_screen('rooms'):
            self.root.ids.sm.add_widget(Builder.load_file(os.path.join(self.kvpath, 'rooms.kv')))
        if not self.root.ids.sm.has_screen('room_add'):
            self.root.ids.sm.add_widget(Builder.load_file(os.path.join(self.kvpath, 'room_add.kv')))
        #self.screen_names.append('rooms')
        #self.root.ids.spnr.values = self.screen_names

        self.root.ids.sm.get_screen('room_add').ids.room_add_spinner.values = [x['name'] for x in self.persona_info]
        self.root.ids.sm.get_screen('rooms').ids.room_scroll.bind(minimum_height=self.root.ids.sm.get_screen('rooms').ids.room_scroll.setter('height'))

        def addRoomObj(room_obj):
            # get encryption key for room
            ekp, persona = self._getPersona_ekp(room_obj['ekp'])

            if ekp['name'] == 'NTRU-256-default-1':
                V = valens.Valens()
                V.importPubKey(ekp['pubkey'])
                
                aes = AESCipher(self.encpw)
                try:
                    V.importPrivKey(aes.decrypt(ekp['encprivkey']))
                except UnicodeDecodeError:
                    if DEBUG:
                        print('Password changed? Encryption keys no longer valid.')
                    self._deleteRoom(room_obj['id'])
                    self._deletePersona(persona['id'])
                    return

                group_key = V.decryptMsg(room_obj['enckey'])
            
            aes = AESCipher(group_key)
            group_name = aes.decrypt(room_obj['encname'])
            room_id = aes.decrypt(room_obj['encgroup'])
            self.roomkeys[room_id] = group_key

            
            #room = room_obj['title']
            self._addRoom_toWindow(room_obj, room_id, group_name)

        # load one room or initialize and load all rooms (_initUser)
        rooms = json.loads(response.content)
        if roomkey_pk:
            addRoomObj(rooms)
            self._join_room(self.rooms[str(rooms['id'])])
        else:
            self.room_widgets = {}
            self.room_info = []
            self.roomkeys = {}
            # add persona buttons
            self.rooms = {} 
            for room_obj in rooms['results']:
                addRoomObj(room_obj)

    def _roomEncrypt(self, msg, room):
        
        aes = AESCipher(self.roomkeys[room])
        return aes.encrypt(msg)
    
    def _roomDecrypt(self, msg, room):
        
        aes = AESCipher(self.roomkeys[room])
        return aes.decrypt(msg)

    #### Init User Interface ####
    
    def _initUser(self):
        
        # first login
        if not hasattr(self, 'loggedout_spinner'): #'loggedout_spinner' in self.root.ids:
            self.root.ids.av.remove_widget(self.root.ids.loggedout_spinner)
        
        if hasattr(self, 'loggedout_spinner'):
            # logins after logouts
            self.root.ids.av.remove_widget(self.loggedout_spinner)
        
        self.user_spnr = Builder.load_file(os.path.join(self.kvpath, 'user_btn.kv'))
        self.root.ids.av.add_widget(self.user_spnr)
        
        # user screen
        self.root.ids.sm.add_widget(Builder.load_file(os.path.join(self.kvpath, 'user.kv')))
        #self.screen_names.append('user')
        #self.root.ids.spnr.values = self.screen_names
        
        #box = BoxLayout(orientation='horizontal')
        
        # setup persona button
        self._getPersonas()
        pbtn = Button(text='Personas',
                     size_hint=(1,0.25))
                     #height=dp(90))
        pbtn.bind(on_press=partial(self.go_screen_name, 'personas'))
        #box.add_widget(pbtn)
        self.root.ids.sm.get_screen('user').ids.user_scroll.add_widget(pbtn)

        # room button
        self._getRooms()
        rbtn = Button(text='Rooms',
                     size_hint=(1,0.25))
                     #height=dp(90))
        rbtn.bind(on_press=partial(self.go_screen_name, 'rooms'))
        #box.add_widget(rbtn)
        self.root.ids.sm.get_screen('user').ids.user_scroll.add_widget(rbtn)

        # settings button
        sbtn = Button(text='Settings',
                     size_hint=(1,0.25))
                     #height=dp(90))
        #sbtn.bind(on_press=self.logout)
        #box.add_widget(lbtn)
        self.root.ids.sm.get_screen('user').ids.user_scroll.add_widget(sbtn)
        
        # logout button
        lbtn = Button(text='Logout',
                     size_hint=(1,0.25))
                     #height=dp(90))
        lbtn.bind(on_press=self.logout)
        #box.add_widget(lbtn)
        self.root.ids.sm.get_screen('user').ids.user_scroll.add_widget(lbtn)
        
        # connect to rooms
        self.connect()

        # set screen
        self.go_screen_name('user')

    def _getCookies(self, domain):
        cookie_jar = self.session.cookies
        cookie_dict = cookie_jar.get_dict(domain=domain)
        found = ['%s=%s' % (name, value) for (name, value) in cookie_dict.items()]
        return ';'.join(found)
    
    def _popup(self, content, title='Information'):
        
        c = Button(text=content)
        popup = Popup(title=title,
                      content=c)
        c.bind(on_press=popup.dismiss)
        popup.open()
    
    def _confirmation(self, dkey, msg, ok_callback, cancel_callback=None):
        
        lbl = Label(text=msg,
                    size_hint_y=None,
                    halign='center')

        btn_ok = Button(text='OK')
        btn_cancel = Button(text='Cancel')

        box = BoxLayout(size_hint_y=None)

        box.add_widget(btn_ok)
        box.add_widget(btn_cancel)
        
        grid = GridLayout(rows=2)
        grid.add_widget(lbl)
        grid.add_widget(box)
        
        popup = Popup(title='Confirmation',
                      content=grid)
    
        if cancel_callback:
            btn_cancel.bind(on_press=cancel_callback)
        else:
            btn_cancel.bind(on_press=popup.dismiss)
        
        btn_ok.bind(on_press=ok_callback)

        # if self.popup does not exist, create it
        if not hasattr(self, 'popup'):
            self.popup = {}

        # add this popup (so we have the handle to dismiss it, etc)
        self.popup[dkey] = popup
        self.popup[dkey].open()

    def _notification_handler(self, msg):
        #print('Received msg: {}'.format(msg))
        if 'action' not in msg:
            if DEBUG:
                print('Unrecognized message: {}'.format(msg))
            return

        if msg['action'] == 'invite':
            if 'roomkey' not in msg or 'inviter' not in msg:
                if DEBUG:
                    print('Bad invitation: {}'.format(msg))
                return
            
            def confirm(data, *args):
                
                # populate RoomKey for other
                URL = "{}://{}:{}/api/roomkey/".format(PROTO, self.host, PORT)
                csrftoken = self._getCSRFtoken(URL)
                data['csrfmiddlewaretoken'] = self.session.cookies['csrftoken']
                response = self.session.post(URL,
                                             data=data,
                                             files={},
                                             headers = {
                                              'Referer': URL,
                                              'X-CSRFToken': csrftoken,
                                             })
                
                if response.status_code != 201:
                    self._popup('There was a problem')
                    return
        
                # add room to window
                room_data = json.loads(response.content)
                self._getRooms(room_data['id'])
                self.go_screen_name('rooms')
                
                self.popup['roomkey-{}'.format(data['encgroup'])].dismiss()
                del self.popup['roomkey-{}'.format(data['encgroup'])]
            def cancel(data, *args):
                #self._deleteRoom(rk_pk)
                self.popup['roomkey-{}'.format(data['encgroup'])].dismiss()
                del self.popup['roomkey-{}'.format(data['encgroup'])]
            self._confirmation('roomkey-{}'.format(msg['roomkey']['encgroup']), 
                        'Accept invitation from {}?'.format(msg['inviter']), 
                        partial(confirm,msg['roomkey']), 
                        partial(cancel,msg['roomkey']))
            #self._getRooms(roomkey_pk=msg['roomkey'])

        elif msg['action'] == 'reject':
            if 'description' not in msg:
                if DEBUG:
                    print('Bad rejection: {}'.format(msg))
            self._popup(msg['description'])
    
    def _show_load_file(self, room, *args):

        container = BoxLayout(orientation='vertical')
        popup = Popup(title="Load file",
                      content=container)

        filechooser = FileChooserListView(pos=self.root.pos,
                                          path=getattr(self, 'user_data_dir'),
                                          size_hint=(1,1))

        cancel_btn = ActionButton(text='Cancel')
        cancel_btn.bind(on_press=popup.dismiss)
        open_btn = ActionButton(text='Open')
        open_btn.bind(on_release=partial(self._load_file, room, filechooser, popup))
        box = BoxLayout(orientation='horizontal',
                        height=dp(90),
                        size_hint_y=None)
        box.add_widget(cancel_btn)
        box.add_widget(open_btn)

        container.add_widget(filechooser)
        container.add_widget(box)
        
        popup.open()

    def _load_file(self, room, filechooser, popup, *args):
        if DEBUG:
            print('room: {}'.format(room))
            print('selection: {}'.format(filechooser.selection))
        popup.dismiss()

        for filepath in filechooser.selection:
            if not os.path.isdir(filepath):
                fname = os.path.basename(filepath)
                if os.stat(filepath).st_size < 1000000:
                    
                    with open(filepath, "rb") as imageFile:
                        img_str = base64.b64encode(imageFile.read())
                        enc_img_str = self._roomEncrypt(img_str.decode('utf-8'), room).decode('utf-8')
                        try:
                            self.ws.send(
                                json.dumps({
                                    'command': 'sendfile',
                                    'room': room,
                                    'fname': self._roomEncrypt(fname, room).decode('utf-8'),
                                    'message': enc_img_str,
                                })
                            )
                        except WebSocketConnectionClosedException:
                            self._popup('Connection lost')
                            self.logout()
                            return

                # file is too large
                else:
                    self._popup('{}: too large'.format(fname))

    def on_stop(self):
        exit()


if __name__ == '__main__':
    Cyber5KApp().run()

